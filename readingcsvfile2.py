import csv
#importing CSV library

file_name = "Document_read.csv"
#file_name


with open(file_name, mode='r') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Columns names are {", ".join(row)}')
            line_count = line_count + 1
        else:
            print(row)
            line_count = line_count + 1
    print(f'Total no of lines {line_count}')

csv_file.close()
